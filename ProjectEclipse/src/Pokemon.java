import java.util.Random;

public class Pokemon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Random r = new Random();
		System.out.println(r.nextInt(32));
		
		int atk = r.nextInt(31)+1;
		int def = r.nextInt(31)+1;
		int vel = r.nextInt(31)+1;
		int spc = r.nextInt(31)+1;
		
		String tipus1 = "Fuego";
		String tipus2 = "Elec";
		int npokedex = r.nextInt(151)+1;
		boolean Shiny = r.nextBoolean();
		
		////A  Mitja mes gran que 20
		double media = (atk+def+vel+spc)/4;
		if(media>=20) {
		System.out.println("cumple A");
		}else {
		System.out.println("no cumple A");
		}
			
		///B Tot mes gran que 16
		if(atk>=16&&def>=16&&vel>=16&&spc>=16) {
			System.out.println("cumple B");
		}else {
			System.out.println("No cumple B");
		}
		
		///C Tipus1 fuego y no pokemon inicial
		if (tipus1 == "Fuego" && npokedex >= 133 && npokedex <= 143){
			System.out.println("cumple C");	
		}else {
			System.out.println("no cumple C");
		}
		
		///D 1 dels 2 tipus gel
		if (tipus1 == "Hielo" || tipus2 == "Hielo") {
			System.out.println("cumple D");
		}else {
			System.out.println("no cumple D");
		}
		
		///E tipus 1 dragon
		if (tipus1 == "Dragon" && tipus2 == null) {
			System.out.println("cumple E");
		}else {
			System.out.println("no cumple E");
		}
		
		///F tipo veneno y en spc +18
		if ((tipus1 == "Veneno" || tipus2 == "Veneno") && spc > 18)  {
			System.out.println("cumple F");
		}else {
			System.out.println("no cumple F");
		}
		
		///G tipo Lucha atk>def
		if ((tipus1 == "Lucha" && tipus2 == "Lucha") && atk > def)  {
			System.out.println("cumple G");
		}else {
			System.out.println("no cumple G");
		}
		
		///H tipo Acero & (25+atak & 25+def) or 27+spc & no Legendario
		if ((tipus1 == "Acero" || tipus2 == "Acero") && ((atk > 25 && def > 25)|| (spc > 27)) && npokedex < 144)  {
			System.out.println("cumple H");
		}else {
			System.out.println("no cumple H");
		}
		
		///I 
		if (((tipus1 == "Acero" || tipus2 == "Acero") && ((atk > 25 && def > 25)|| (spc > 27)) && npokedex < 144) || Shiny == true)  {
			System.out.println("cumple I");
		}else {
			System.out.println("no cumple I");
		}
		
		
	}
	}


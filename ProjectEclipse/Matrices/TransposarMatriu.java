import java.util.Scanner;

public class TransposarMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int numero_veces = sc.nextInt();
		for (int i = 0; i < numero_veces; i++) {
			int fila = sc.nextInt();
			int colum = sc.nextInt();
			int[][] mat = new int[fila][colum];
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					mat[f][c] = sc.nextInt();

				}

			}
			for (int c = 0; c < mat[0].length; c++) {
				for (int f = 0; f < mat.length; f++) {
					System.out.print(mat[f][c]+ " ");

				}
				System.out.println();

			}

		}
	}

}

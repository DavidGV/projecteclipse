import java.util.Scanner;

public class QuadratLlati {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int numero_veces = sc.nextInt();
		for (int i = 0; i < numero_veces; i++) {
			int num = sc.nextInt();
			int[][] mat = new int[num][num];
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					if (f == 0) {
						mat[f][c] = c + 1;
					} else {
						if (mat[f - 1][c] == 1) {
							mat[f][c] = num;
						} else {
							mat[f - 1][c] = (mat[f - 1][c] - 1);
						}

					}
				}

				for (int fil = 0; fil < mat.length; fil++) {
					for (int c = 0; c < mat[0].length; c++) {
						System.out.print(mat[fil][c] + " ");
					}
					System.out.println();
				}

			}
		}

	}
}
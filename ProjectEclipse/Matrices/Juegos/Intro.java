package Juegos;
import java.util.Random;

public class Intro {
	
	public static void main(String[] args) {
		
		///declarar matrices
		///Norma Fundamental de las Matrices:
		//PRIMERO FILAS, LUEGO COLUMNAS,
		int[][] mat1 = { {1,2,3} , {4,5,6} , {7,8,9} };
		int[][] mat2 = new int[3][4];
		
		//rellenar matriz
		
		///BUCLE UNIVERSAL DE LAS MATRICES
		//Numero de filas de una matriz:  mat.length 
		//Numero de columnas de una matriz: mat[0].length
		//array[f][c];
		for (int f = 0; f < mat1.length; f++) {	
			for (int c = 0; c < mat1[0].length; c++) {
				System.out.print(mat1[f][c]+" ");
			}
			System.out.println();
		}
		
		System.out.println("--------------------");
		
		
		///rellenar matriz
		Random r = new Random();
		for (int f = 0; f < mat2.length; f++) {	
			for (int c = 0; c < mat2[0].length; c++) {
				mat2[f][c]=r.nextInt(10);
			}
		}
		
		///volvemos a imprimir matriz copypaste legendario
		for (int f = 0; f < mat2.length; f++) {	
			for (int c = 0; c < mat2[0].length; c++) {
				System.out.print(mat2[f][c]+" ");
			}
			System.out.println();
		}
		System.out.println("--------------------");
		
		
		///Contar Numeros Mas grandes que 5
		int contador = 0;
		for (int f = 0; f < mat2.length; f++) {	
			for (int c = 0; c < mat2[0].length; c++) {
				if(mat2[f][c]>=5) {
					contador++;
				}
			}
		}
		System.out.println(contador);
		
		
		
		
		
		
	}

}

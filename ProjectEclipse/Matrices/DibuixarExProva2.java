import java.util.Scanner;

public class DibuixarExProva2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		boolean salir = false;
		char[][] mat = new char[0][0];
		while (!salir) {
			System.out.println("0.Sortir 1.Dimensionar 2.Veure Tauler 3.Vertical 4.Rectangle 5.Limits");
			int opcio = sc.nextInt();
			switch (opcio) {
			case 1:
				System.out.println("Introducir files");
				int fila = sc.nextInt();
				System.out.println("Introducir columnes");
				int colum = sc.nextInt();
				mat = new char[fila][colum];
				for (int f = 0; f < mat.length; f++) {
					for (int c = 0; c < mat[0].length; c++) {
						mat[f][c] = '.';
					}

				}
				break;
			case 2:
				for (int f = 0; f < mat.length; f++) {
					for (int c = 0; c < mat[0].length; c++) {
						System.out.print(mat[f][c]+ " ");

					}
					System.out.println();

				}
				break;
			case 3:
				System.out.println("col");
				int col = sc.nextInt();
				System.out.println("filaInicial");
				int filaInici = sc.nextInt();
				System.out.println("filaFinal");
				int filaFinal = sc.nextInt();
				for (int f = 0; f < mat.length; f++) {
					for (int c = 0; c < mat[0].length; c++) {
						if(c == col && filaInici<=f && f<=filaFinal ) {
						mat[f][c] = 'X';	
						}
						
					}

				}
				break;	
			case 4:
				System.out.println("f1");
				int f1 = sc.nextInt();
				System.out.println("c1");
				int c1 = sc.nextInt();
				System.out.println("f2");
				int f2 = sc.nextInt();
				System.out.println("c2");
				int c2 = sc.nextInt();
				if (c1>c2) {
					int c1r = c1;
					c1 = c2;
					c2 = c1r;
				}
				if (f1>f2) {
					int f1r = f1;
					f1 = f2;
					f2 = f1r;
				}
				for (int f = 0; f < mat.length; f++) {
					for (int c = 0; c < mat[0].length; c++) {
						if(c1 <= c && c <= c2 && f1<=f && f<=f2 ) {
						mat[f][c] = 'X';
						}
					}

				}
				break;	
			case 5:
				for (int f = 0; f < mat.length; f++) {
					for (int c = 0; c < mat[0].length; c++) {
						if(f == 0 || c == 0 || f == mat.length-1 || c == mat[0].length-1) {
						mat[f][c] = 'X';	
						}else {
						mat[f][c] = '.';
						}
						
					}

				}
				break;	
			case 0:
				System.out.println("Sortir");
				salir = true;
				break;
			default:
				System.out.println("Eres retrasado mental, vas con m�s retraso que los de canarias");
			}
		}

	}
}

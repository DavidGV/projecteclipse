import java.util.HashMap;

public class Peparacio {
	static int[][] board = new int[7][7];
	static Board b = new Board();
	static Window w = new Window(b);
	static int puntosB, puntosN = 0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		init();
		init2();
		while(true) {
			HashMap<String, Integer> posicion = new HashMap<>();
			view();
			view2();
		}
		
	}
	private static void view2() {
		// TODO Auto-generated method stub
		for (int f = 0; f < board.length; f++) {
			for (int c = 0; c < board[0].length; c++) {
				System.out.print(board[f][c]);
			}
			System.out.println();
		}
	}
	private static void init2() {
		// TODO Auto-generated method stub
		for (int f = 0; f < board.length; f++) {
			for (int c = 0; c < board[0].length; c++) {
				System.out.print(board[f][c]=0);
			}
			System.out.println();
		}
	}
	private static void init() {
		// TODO Auto-generated method stub
		board = borde(board, 3);
		b.setColorbackground(0xfed8a7);
		b.setActborder(true);
		String[] sprites = { "", "goWhite.png", "goBlack.png", "swap.png" };
		b.setSprites(sprites);
		b.draw(board);
	}
	private static int[][] borde(int[][] tablero, int valor) {
		// TODO Auto-generated method stub
		int[][] mborde = new int[tablero.length + 2][tablero[0].length + 2];
		for (int f = 0; f < mborde.length; f++) {
			for (int c = 0; c < mborde[0].length; c++) {
				if (f == 0 || f == mborde.length - 1 || c == 0 || c == mborde[0].length - 1) {
					mborde[f][c] = valor;
				} else {
					mborde[f][c] = tablero[f - 1][c - 1];
				}
			}
		}
		return mborde;
	
		
	}
	private static HashMap<String, Integer> seleccionarPosicion(int turno) {
		// TODO Auto-generated method stub
		int leftCol = -1;
		int leftRow = -1;
		do {

			try {
				Thread.sleep(50); /// donem una mica de retard per no colapsar el buffer del mouse.
			} catch (InterruptedException e) {
			}

			leftCol = b.getCurrentMouseCol();
			leftRow = b.getCurrentMouseRow();
			if (leftCol != -1 && leftRow != -1) {
				System.out.println("S'ha apretat el boto esquerra en la fila " + leftRow + " columna " + leftCol);
			}
		} while (leftCol == -1);
		HashMap<String, Integer> posicion = new HashMap<>();
		posicion.put("fila", leftRow);
		posicion.put("col", leftCol);
		return posicion;
	}
	private static void view() {
		// TODO Auto-generated method stub
		b.draw(board);
		System.out.println("puntos blanco : " + puntosB + " puntos negro : " + puntosN);
	}

}

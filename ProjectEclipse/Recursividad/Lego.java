import java.util.ArrayList;
import java.util.Scanner;

public class Lego {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// TODO Auto-generated method stub
		int numeroVeces = sc.nextInt();
		for (int i = 0; i < numeroVeces; i++) {
			int lado = sc.nextInt();
			System.out.println((recursivo(lado)));
			
		}
	}

	private static int recursivo(int lado) {
		// TODO Auto-generated method stub
		if (lado == 1) {
			
			return 1;
		}else {
			
			return lado * 2 + (lado-2) *2 + recursivo(lado - 1);
		}
		
	}

}

import java.util.Scanner;

public class ExamenPruebaFunciones {
	static Scanner sc = new Scanner(System.in); // static porque es EL scanner
	static int tablero[][] = new int[4][4]; // static tablero porque es EL
											// tablero, solo hay uno.
	static boolean c1 = false; // static porque necesito que entre
								// muchas veces en turno1()
	static boolean c2 = false;
	static boolean c3 = false;
	static boolean c4 = false;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while (true) {
			System.out.println("1.Configuracion 2.Instrucciones 3.Jugar");
			int opcion = sc.nextInt();
			sc.nextLine();
			switch (opcion) {
			case 1:
				configuracio();
				break;
			case 2:
				instrucciones();
				break;
			case 3:
				iniciarTablero();
				jugar();
				c1 = false;
				c2 = false;
				c3 = false;
				c4 = false;
				break;
			default:
				break;
			}
		}
	}

	private static void jugar() {
		// TODO Auto-generated method stub
		while (true) {
			turno1();
			turno2();
		}
	}

	private static void iniciarTablero() {
		// TODO Auto-generated method stub
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				tablero[i][j] = 0;
			}
		}
	}

	private static void turno2() {
		// TODO Auto-generated method stub

	}

	private static void turno1() {
		// TODO Auto-generated method stub
		System.out.println("Jugador1 Dime en que columna quieres poner la ficha");
		int columna = sc.nextInt();
		if (columna == 1) {
			if (!c1) {
				tablero[3][columna - 1] = 1;
				c1 = true;
			} else {
				for (int i = 0; tablero.length-1 <= 0; i++) {
					
				}
			}
		} else if (columna == 2) {
			if (!c2) {
				tablero[3][columna - 1] = 1;
				c2 = true;
			} else {

			}
		} else if (columna == 3) {
			if (!c3) {
				tablero[3][columna - 1] = 1;
				c3 = true;
			} else {

			}
		} else if (columna == 4) {
			if (!c4) {
				tablero[3][columna - 1] = 1;
				c4 = true;
			} else {

			}
		}

	}

	private static void instrucciones() {
		// TODO Auto-generated method stub
		System.out.println(
				"En aquest examen creareu un Connect 4 fent servir programació modular i amb el paradigma Top-Down\n"
						+ "El connecta 4 es un joc a on s’han de posar en linea 4 fitxes del mateix color. Per torns els jugadors introdueixen una fitxa en la columna que vulguin i aquesta caurà a la posició més baixa. Guanya la partida el primer en alinear 4 fitxes del mateix color. Si totes les columnes queden plenes però ningú ha guanyat es dona empat.\n"
						+ "Per a simplificar l’exercici el taulell sobre el que jugareu serà de 4x4\n"
						+ "El joc serà de 2 jugadors, i es farà per torns\n"
						+ "El joc ha de ser capaç de detectar un moviment invàlid, en aquest cas tornaria a demanar el moviment sense passar el torn\n"
						+ "El joc tindra un menu principal, i al haver un guanyador haurà d’anunciar el guanyador i tornar al menú principal. També si hi ha empat\n"
						+ "");
	}

	private static void configuracio() {
		// TODO Auto-generated method stub
		System.out.println("Dime del Jugador1");
		String j1 = sc.nextLine();
		System.out.println("Dime del Jugador2");
		String j2 = sc.nextLine();
	}

}

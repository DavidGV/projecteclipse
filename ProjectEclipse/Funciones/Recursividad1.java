
public class Recursividad1 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int fa = 5;
		int res= factorial(fa);
		System.out.println(res);
		
	}
		
	private static int factorial(int fa) {
		
		if (fa == 1) {
			// caso base
			return 1;
		} else {

			// caso recursivo
			return fa + factorial(fa - 1);
		}
	}
}

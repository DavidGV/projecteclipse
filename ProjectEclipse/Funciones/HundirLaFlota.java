import java.util.Scanner;

public class HundirLaFlota {
	static int tableroOculto[][] = new int[7][7];
	static int tablero[][] = new int[7][7];
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		init();
		boolean victoria = false;
		while (!victoria) {
			int x = decirx();
			int y = deciry();
			comporbar(x,y);
			mostrarTablero();
			victoria = comprobarVictoria();
		}
		System.out.println("Has ganado");
	}

	private static boolean comprobarVictoria() {
		// TODO Auto-generated method stub
		boolean victoria = true;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				if (tableroOculto[i][j] != 0) {
					victoria = false;
				}
			}
			
		}
		return victoria;
	}

	private static void comporbar(int x, int y) {
		// TODO Auto-generated method stub
		boolean hundir1 = true;
		boolean hundir2 = true;
		if(tableroOculto[x][y] == 0) {
			System.out.println("Agua");
			tablero[x][y] = 9;
		} else if(tableroOculto[x][y] == 1) {
			tableroOculto[x][y] = 0;
			tablero[x][y] = 1;
			for (int i = 0; i < tablero.length; i++) {
				for (int j = 0; j < tablero[0].length; j++) {
					if (tableroOculto[i][j] == 1) {
						hundir1 = false;
					}
				}
				
			} if(!hundir1) {
				System.out.println("Tocado barco 1");
			} else {
				System.out.println("Hundido barco 1");
			}
			
		} else if(tableroOculto[x][y] == 2) {
			tableroOculto[x][y] = 0;
			tablero[x][y] = 2;
			for (int i = 0; i < tablero.length; i++) {
				for (int j = 0; j < tablero[0].length; j++) {
					if (tableroOculto[i][j] == 2) {
						hundir2 = false;
					}
				}
				
			} if(!hundir2) {
				System.out.println("Tocado barco 2");
			} else {
				System.out.println("Hundido barco 2");
			}
			
		}
	}

	private static int decirx() {
		// TODO Auto-generated method stub
		System.out.println("Dime las cordenadas x");
		int x = sc.nextInt();
		return x-1;
	}

	private static int deciry() {
		// TODO Auto-generated method stub
		System.out.println("Dime las cordenadas y");
		int y = sc.nextInt();
		return y-1;

	}

	private static void mostrarTablero() {
		// TODO Auto-generated method stub
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j]);
			}
			System.out.println();
		}
		System.out.println("----------------------------------");
	}

	private static void init() {
		// TODO Auto-generated method stub
		for (int i = 0; i < tableroOculto.length; i++) {
			for (int j = 0; j < tableroOculto[0].length; j++) {
				tableroOculto[i][j] = 0;
			}
		}
		tableroOculto[2][1] = 1;
		tableroOculto[2][2] = 1;
		tableroOculto[2][3] = 1;
		tableroOculto[5][3] = 2;
		tableroOculto[6][3] = 2;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				tablero[i][j] = 0;
			}
		}
	}

}

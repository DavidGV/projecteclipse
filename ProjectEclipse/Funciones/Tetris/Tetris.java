package Tetris;


import java.util.Random;
import java.util.Scanner;

public class Tetris {
	
	///tablero
	static int[][] tablero = new int[20][10];
	//fichas
	static int[][] o = { {1,1},{1,1} };
	static int[][] t = { {0, 0, 0}, {0, 2, 0}, {2, 2, 2}};
	static int[][][] fichas = { o, t };
	
	static int[][] ficha;
	static int x,y=0;
	
	static Board b = new Board();
	static Window w = new Window(b);
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		//antes del bucle
		init();
		view();
		while(true) {
			move();
			/*if(aterrizaje()) {
				if(comprobarLinea()) {
					eliminarLinea();
				}
				if(arribaDelTodo()) {
					loser=true;
				}
				generarFicha();
			}*/
			view();
			sc.nextLine();
		}
		
		
		/*
		boolean loser = false;
		while(!loser) {
			move();
			if(aterrizaje()) {
				if(comprobarLinea()) {
					eliminarLinea();
				}
				if(arribaDelTodo()) {
					loser=true;
				}
				generarFicha();
			}
			view();
		}
		*/
		
	}

	private static void view() {
		// TODO Auto-generated method stub
		b.draw(tablero);
	}

	private static void generarFicha() {
		Random r = new Random();
		int rand = r.nextInt(fichas.length);
		ficha = fichas[rand];
		
		x = 0;
		y = 4;
		
		for(int f = 0; f <ficha.length; f++) {
			for(int c = 4; c <4+ficha[0].length; c++) {
				tablero[f][c]=ficha[f][c-4]; 
			}
		}
	}

	private static boolean arribaDelTodo() {
		return false;
	}

	private static void eliminarLinea() {
		// TODO Auto-generated method stub
		
	}

	private static boolean comprobarLinea() {
		// TODO Auto-generated method stub
		return false;
	}

	private static boolean aterrizaje() {
		///aterrizaje
		
		///es mu complicao.
		
		
		
		return false;
	}

	private static void move() {
		// TODO Auto-generated method stub

		borrarFicha(x,y,ficha);
		x++;
		escribirFicha(x,y,ficha);
		
		
	}

	private static void escribirFicha(int x2, int y2, int[][] ficha2) {
		// TODO Auto-generated method stub
		for (int i = 0; i < ficha2.length; i++) {
			for (int j = 0; j < ficha2[0].length; j++) {
				//TODO recorres tablero y escribes si en la posicion de la ficha hubiera un 1
				if (ficha[i][j]!=0) {
					tablero[i+x][j+y]=ficha2[i][j];
				}
			}
		}
	}

	private static void borrarFicha(int x2, int y2, int[][] ficha2) {
		for (int i = 0; i < ficha2.length; i++) {
			for (int j = 0; j < ficha2[0].length; j++) {
				//TODO recorres tablero y borras si en la posicion de la ficha hubiera un 1
				if (ficha2[i][j]!=0) {
					tablero[i+x][j+y]=0;
				}
			}
		}
		
	}

	private static void init() {
		
		b.setActcolors(true);
		b.setActsprites(false);
		b.setActtext(false);
		int[] colors = { 0x0000FF, 0x00FF00, 0xFFFF00, 0xFF0000, 0xFF00FF, 0x00FFFF, 0x000000, 0xFFFFFF, 0xFF8000,
				0x7F00FF };
		b.setColors(colors);
		
		
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				tablero[i][j]=0;
			}
		}
		generarFicha();
	}

}

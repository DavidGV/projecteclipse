import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class ExamenPruebaFicheros {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		//Clase File. La construyes pasandole el path
		fun1();
		fun2();
		
		
		/*
	    while (br2.ready()) {
	    	nom2 = br2.readLine();
	  	    raza2 = br2.readLine();
	  	    edat2 = Integer.parseInt(br2.readLine());
	  	    menjarPreferit2 = br2.readLine();
	  	    capat2 = Boolean.parseBoolean(br2.readLine());
	  	   
	  	    Gat gat2 = new Gat (nom2, raza2, edat2, menjarPreferit2, capat2);
	  	    oos2.writeObject(gat2);
			//mete un salto de linea en le buff�
	
		}
	    br2.close();
	
		oos2.flush();
		oos2.close();*/
		
	    
	}

	private static void fun2() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		File f = new File("ficheros/gats.txt");
		if(!f.exists()) f.createNewFile();
		FileReader in = new FileReader(f);
		BufferedReader br = new BufferedReader(in);
		  
		String nom;
	    String raza;
	    int edat;
	    String menjarPreferit;
	    boolean capat;
	    
	    ArrayList<Gat> gatos = new ArrayList<Gat>();
	    
	    while (br.ready()) {
	    nom = br.readLine();
	    raza = br.readLine();
	    edat = Integer.parseInt(br.readLine());
	    menjarPreferit = br.readLine();
	    capat = Boolean.parseBoolean(br.readLine());
	    
	    Gat gat = new Gat (nom, raza, edat, menjarPreferit, capat);
	    gatos.add(gat);
	    }
	    br.close();
	    
	    // Escriure serie
	    File f2 = new File("ficheros/gats.dat");
		FileOutputStream fos = new FileOutputStream(f2);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
	    
		for (Gat gaat: gatos) {
			oos.writeObject(gaat);
		}
	    
		oos.flush();
		oos.close();
		
		//Lectura Serie
		
		File f5 = new File("ficheros/gats.dat");
		FileInputStream fis5 = new FileInputStream(f5);
		ObjectInputStream ois5 = new ObjectInputStream(fis5);
		
		for (Gat gaat: gatos) {
			Object o = ois5.readObject();
			Gat g2 = (Gat) o;
			System.out.println(g2);
		}
		
		
		
		//Escribir a fichero otra vezfd
		
		File f3 = new File("ficheros/gats2.txt");
		if(!f.exists()) f.createNewFile();
		FileWriter out = new FileWriter(f3);
		BufferedWriter bw = new BufferedWriter(out);
		
		bw.write(g2.nom);
		bw.newLine();
		bw.write(g2.raza);
		bw.newLine();
		bw.write(g2.edat+"");
		bw.newLine();
		bw.write(g2.menjarPreferit);
		bw.newLine();
		bw.write(g2.capat+"");
		bw.newLine();
		
	
		bw.flush();
		bw.close();
	}

	private static void fun1() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		File f = new File("ficheros/gat.txt");
		if(!f.exists()) f.createNewFile();
		FileReader in = new FileReader(f);
		BufferedReader br = new BufferedReader(in);
		  
		String nom;
	    String raza;
	    int edat;
	    String menjarPreferit;
	    boolean capat;
	    
	    nom = br.readLine();
	    raza = br.readLine();
	    edat = Integer.parseInt(br.readLine());
	    menjarPreferit = br.readLine();
	    capat = Boolean.parseBoolean(br.readLine());
	    br.close();
	    
	    Gat gat = new Gat (nom, raza, edat, menjarPreferit, capat);
	    
	    // Escriure serie
	    File f2 = new File("ficheros/gat.dat");
		FileOutputStream fos = new FileOutputStream(f2);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
	    
	    oos.writeObject(gat);
	  
		oos.flush();
		oos.close();
		
		//Lectura Serie
		
		File f5 = new File("ficheros/gat.dat");
		FileInputStream fis5 = new FileInputStream(f5);
		ObjectInputStream ois5 = new ObjectInputStream(fis5);
		
		Object o = ois5.readObject();
		Gat g2 = (Gat) o;
		System.out.println(g2);
		
		
		//Escribir a fichero otra vez
		
		File f3 = new File("ficheros/gat2.txt");
		if(!f.exists()) f.createNewFile();
		FileWriter out = new FileWriter(f3);
		BufferedWriter bw = new BufferedWriter(out);
		
		bw.write(g2.nom);
		bw.newLine();
		bw.write(g2.raza);
		bw.newLine();
		bw.write(g2.edat+"");
		bw.newLine();
		bw.write(g2.menjarPreferit);
		bw.newLine();
		bw.write(g2.capat+"");
		bw.newLine();
		
	
		bw.flush();
		bw.close();
	}

}

import java.io.Serializable;

public class Gat implements Serializable {
	String nom;
    String raza;
    int edat;
    String menjarPreferit;
    boolean capat;
	public Gat(String nom, String raza, int edat, String menjarPreferit, boolean capat) {
		super();
		this.nom = nom;
		this.raza = raza;
		this.edat = edat;
		this.menjarPreferit = menjarPreferit;
		this.capat = capat;
	}
}

import java.util.Random;
import java.util.Scanner;

public class BuscaMinas {
	static Scanner sc = new Scanner(System.in);
	static Random r = new Random();
	static int[][] tablero;
	static int[][] tableroMinas;
	static int filas;
	static int columnas;
	static int minas;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean salir = false;
		while (!salir) {
			System.out.println("0.Salir 1.Mostrar Ayuda 2.Opciones 3.Jugar Partida 4.Ver Rankings");
			int opcio = sc.nextInt();
			sc.nextLine();
			switch (opcio) {
			case 1:
				mostrarAyuda();

				break;
			case 2:
				opcion();
				inicializacionMines();
				inicializacionCampo();
				viewMines();
				viewCampo();
			

				break;
			case 3:
				jugar();

				break;
			case 4:
				rankings();
				break;
			case 0:
				System.out.println("Salir");
				salir = true;
				break;
			default:
				System.out.println("Eres retrasado mental, vas con más retraso que los de canarias");
			}
		}
	}

	private static void viewCampo() {
		// TODO Auto-generated method stub
		for (int fi = 0; fi < tablero.length; fi++) {
			for (int co = 0; co < tablero[0].length; co++) {
				System.out.print(tablero[fi][co]);

			}
			System.out.println("");

		}
		System.out.println("----------------------------------------------");
	}

	private static void viewMines() {
		// TODO Auto-generated method stub
		for (int fi = 0; fi < tablero.length; fi++) {
			for (int co = 0; co < tablero[0].length; co++) {
				System.out.print(tableroMinas[fi][co]);

			}
			System.out.println("");

		}
		System.out.println("----------------------------------------------");
	}

	private static void opcion() {
		// TODO Auto-generated method stub
		System.out.println("Escribe tu nombre");
		String nombreJugador = sc.nextLine();
		System.out.println("Numero filas");
		filas = sc.nextInt();
		System.out.println("Numero columnas");
		columnas = sc.nextInt();
		System.out.println("Numero minas");
		minas = sc.nextInt();

	}

	private static void inicializacionMines() {
		// TODO Auto-generated method stub
		tablero = new int[filas][columnas];
		tableroMinas = new int[filas][columnas];

		while (minas != 0) {
			int rx = r.nextInt(filas);
			int ry = r.nextInt(columnas);
			if (tableroMinas[rx][ry] == 0) {
				tableroMinas[rx][ry] = 1;
				minas--;
			}
		}
	}

	private static void inicializacionCampo() {
		// TODO Auto-generated method stub
		tablero = new int[filas][columnas];
		for (int fi = 0; fi < tablero.length; fi++) {
			for (int co = 0; co < tablero[0].length; co++) {
				tablero[fi][co] = 9;

			}

		}

	}

	private static void rankings() {
		// TODO Auto-generated method stub

	}

	private static void jugar() {
		// TODO Auto-generated method stub
		
	}

	private static void mostrarAyuda() {
		// TODO Auto-generated method stub

	}

}


public class Perro extends Raza {
	
	public Perro(String nombreRaza, PerroTamano tamano, int tempsVida, int edad, double altura, String nombre) {
		super(nombreRaza, tamano, tempsVida);
		// TODO Auto-generated constructor stub
		perros++;
	}
	static int perros;
	int edad;
	double altura;
	String nombre;
	/*
	public Perro(String nombre) {
		perros++;
		this.nombre = nombre;
		this.edad = 5;
		this.altura = 2.0;
	}
	
	public Perro(Perro perro) {
		perros++;
		this.edad = perro.edad;
		this.altura = perro.altura;
		this.nombre = perro.nombre;
	}

	public Perro() {
		perros++;
	}
	public void clonar(Perro perro) {
		this.edad=perro.edad;
		this.altura = perro.altura;
		this.nombre = perro.nombre+"Copia";
	}*/
	//public static Perro p = null;
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/*
	public static Perro getPerro() {
		if(p==null) {
			p = new Perro();
			return p;
		}else {
			return p;
		}
		
	}
	*/
	public void ladrar() {
		System.out.println("Guau");
	}
	@Override
	public String toString() {
		return "Perro [edad=" + edad + ", altura=" + altura + ", nombre=" + nombre + ", nombreRaza=" + nombreRaza
				+ ", tamano=" + tamano + ", tempsVida=" + tempsVida + "]";
	}

	
}

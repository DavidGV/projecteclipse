
public class Raza {
	
	public Raza(String nombreRaza, PerroTamano tamano) {
		this.nombreRaza = nombreRaza;
		this.tamano = tamano;
		this.tempsVida = 1;
	}
	String nombreRaza;
	public String getNombreRaza() {
		return nombreRaza;
	}
	public void setNombreRaza(String nombreRaza) {
		this.nombreRaza = nombreRaza;
	}
	public PerroTamano getTamano() {
		return tamano;
	}
	public void setTamano(PerroTamano tamano) {
		this.tamano = tamano;
	}
	public int getTempsVida() {
		return tempsVida;
	}
	public void setTempsVida(int tempsVida) {
		this.tempsVida = tempsVida;
	}
	PerroTamano tamano;
	int tempsVida;
	public Raza(String nombreRaza, PerroTamano tamano, int tempsVida) {
		this(nombreRaza, tamano);
		this.tempsVida = tempsVida;
		
	}
}

package ExamenSurpraisXD;

public class Motos extends Vehicles{

	public Motos(String matricula, String model, int nAnys, String situacio, int cilindrada) {
		super(matricula, model, nAnys, situacio);
		tipo= 'm';
	}
	public void repara() {
		if (this.nAnys > 2) {
			this.situacio = "Baixa";
		} else {
			if (this.situacio.equals("Disponible")) {
				this.situacio = "Avariada";
			}
		}
	}
}

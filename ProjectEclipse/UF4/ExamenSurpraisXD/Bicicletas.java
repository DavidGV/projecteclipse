package ExamenSurpraisXD;

public class Bicicletas extends Vehicles {

	public Bicicletas(String matricula, String model, int nAnys, String situacio) {
		super(matricula, model, nAnys, situacio);
		tipo= 'b';
	}

	public void repara() {
		if (this.situacio.equals("Disponible")) {
			this.situacio = "Avariada";
		}

	}
}

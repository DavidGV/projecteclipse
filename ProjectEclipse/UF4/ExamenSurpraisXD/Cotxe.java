package ExamenSurpraisXD;

public class Cotxe extends Vehicles {

	public Cotxe(String matricula, String model, int nAnys, String situacio, int places) {
		super(matricula, model, nAnys, situacio);
		tipo= 'c';
	}

	public void repara() {
		if (this.nAnys > 3) {
			this.situacio = "Baixa";
		} else {
			if (this.situacio.equals("Disponible")) {
				this.situacio = "Avariada";
			}
		}
	}
}

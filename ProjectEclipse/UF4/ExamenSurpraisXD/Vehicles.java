package ExamenSurpraisXD;

import java.util.ArrayList;
import java.util.Objects;

public abstract class Vehicles {
	String matricula;
	String model;
	int nAnys;
	String situacio;
	char tipo;
	
	public Vehicles(String matricula, String model, int nAnys, String situacio) {
		super();
		this.matricula = matricula;
		this.model = model;
		this.nAnys = nAnys;
		this.situacio = situacio;
	}
	/*
	public int nVehicles() {
		return this.numVehicles.size();
		
	}
	*/
	@Override
	public String toString() {
		return "Vehicles [matricula=" + matricula + ", model=" + model + ", nAnys=" + nAnys + ", situacio=" + situacio + "]";
	}
	public String visualitzar() {
		return toString();
	}
	public boolean lloga() {
		if (this.situacio.equals("Disponible")) {
			this.situacio = "Llogada";
			return true;
		}
		return false;
	}
	public void retorna() {
		if (this.situacio.equals("Llogada")) {
			this.situacio = "Disponible";			
		}
		
	}
	public abstract void repara();
	
}

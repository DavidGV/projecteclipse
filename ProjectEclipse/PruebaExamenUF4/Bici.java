
public class Bici extends Vehicles {

	public Bici(String matricula, String model, int edad, Situacion situacion) {
		super(matricula, model, edad, situacion);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Bici [matricula=" + matricula + ", model=" + model + ", edad=" + edad + ", situacion=" + situacion
				+ "]";
	}
	@Override
	public String visualitzar() {
		return toString();
	}
	
}


public class Coche extends Vehicles implements aMotor{
	int plazas;

	public Coche(String matricula, String model, int edad, Situacion situacion, int plazas) {
		super(matricula, model, edad, situacion);
		this.plazas = plazas;
	}

	@Override
	public String toString() {
		return "Coche [plazas=" + plazas + ", matricula=" + matricula + ", model=" + model + ", edad=" + edad
				+ ", situacion=" + situacion + "]";
	}
	
	@Override
	public String visualitzar() {
		return toString();
	}
	
	
	
}

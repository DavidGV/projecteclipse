
public abstract class Vehicles {
	String matricula;
	String model;
	int edad;
	Situacion situacion;
	static int contadorVehicles;

	public Vehicles(String matricula, String model, int edad, Situacion situacion) {
		super();
		this.matricula = matricula;
		this.model = model;
		this.edad = edad;
		this.situacion = situacion;
		contadorVehicles++;
	}

	public void darBaja(Vehicles v) {
		if (v.edad > 4) {
			v.situacion = situacion.BAIXA;
		}
	}

	public String visualitzar() {
		return "";
	}

	public void clonar(Vehicles v) {
		this.matricula = v.matricula;
		this.model = v.model;
		this.edad = v.edad;
		this.situacion = v.situacion;
	}

	public boolean lloga(Vehicles v) {
		if (v.situacion == situacion.DISPONIBLE) {
			v.situacion = situacion.LLOGAT;
			return true;
		}
		return false;
	}

	public void retorna(Vehicles v) {
		if (v.situacion == situacion.LLOGAT) {
			v.situacion = situacion.DISPONIBLE;
		}
	}

	public void repara(Vehicles v) {
		if (v.situacion == situacion.DISPONIBLE) {
			v.situacion = situacion.AVARIAT;
			if (v instanceof Coche) {
				if (v.edad > 3) {
					v.situacion = situacion.BAIXA;
				}
			} else if (v instanceof Moto) {
				if (v.edad > 2) {
					v.situacion = situacion.BAIXA;
				}
			} 
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicles other = (Vehicles) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}

}

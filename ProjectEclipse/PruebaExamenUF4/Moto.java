
public class Moto extends Vehicles implements aMotor{
	int cilindrada;

	public Moto(String matricula, String model, int edad, Situacion situacion, int cilindrada) {
		super(matricula, model, edad, situacion);
		this.cilindrada = cilindrada;
	}

	@Override
	public String toString() {
		return "Moto [cilindrada=" + cilindrada + ", matricula=" + matricula + ", model=" + model + ", edad=" + edad
				+ ", situacion=" + situacion + "]";
	}
	@Override
	public String visualitzar() {
		return toString();
	}
	
	
}

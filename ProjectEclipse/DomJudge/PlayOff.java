import java.util.Scanner;

public class PlayOff {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int numero_veces = sc.nextInt();
		for (int i = 0; i < numero_veces; i++) {
			int equipos = sc.nextInt();
			int victorias = sc.nextInt();
			int min = 0;
			int max = 0;
			if (victorias == 1) {
				min = (equipos - 1);
				max = (equipos - 1);
			} else {
				min = (equipos - 1) * victorias;
				max = (equipos - 1) * (victorias + (victorias - 1));
			}
			System.out.println(min);
			System.out.println(max);
		}
	}

}

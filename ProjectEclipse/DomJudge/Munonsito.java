import java.util.Scanner;

public class Munonsito {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numeroVeces = sc.nextInt();
		sc.nextLine();
		for (int i = 0; i < numeroVeces; i++) {
			String dir1 = sc.nextLine();
			String dir2 = sc.nextLine();
			System.out.println(funcion(dir1,dir2));
		}

	}

	public static String funcion(String dir1, String dir2) {
		// TODO Auto-generated method stub
		
		if (dir1.equals(dir2)) {
			if (dir1.equals("ClicE")) {
				return "Disparar";
			} else if (dir1.equals("ClicD")) {
				return "Res";
			} else if (dir1.equals("a")) {
				return dir1;
			} else if (dir1.equals("d")) {
				return dir1;
			} else if (dir1.equals("w")) {
				return dir1;
			} else if (dir1.equals("s")) {
				return dir1;
			}
		} else {
			if ((dir1.equals("a") && dir2.equals("d")) || (dir1.equals("d") && dir2.equals("a"))) {
				return "Res";
			} else if ((dir1.equals("w") && dir2.equals("s")) || (dir1.equals("s") && dir2.equals("w"))) {
				return "Res";
			} else if ((dir1.equals("ClicE") && dir2.equals("ClicD"))) {
				return "Disparar";
			} else if ((dir1.equals("ClicD") && dir2.equals("ClicE"))) {
				return "Apuntar + Disparar";
			} else {
				return dir1+ " + " +dir2;
			}

		}
		return "Res";
	}

}

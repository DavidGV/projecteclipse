import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MunonsitoTest {

	@Test
	void test() {
		String a = "w";
		String b = "w";
		String c = "w";
		assertEquals(c, Munonsito.funcion(a,b));
		
		String a1 = "a";
		String b1 = "a";
		String c1 = "a";
		assertEquals(c1, Munonsito.funcion(a1,b1));
		
		String a2 = "s";
		String b2 = "s";
		String c2 = "s";
		assertEquals(c2, Munonsito.funcion(a2,b2));
		
		String a3 = "d";
		String b3 = "d";
		String c3 = "d";
		assertEquals(c3, Munonsito.funcion(a3,b3));
		
		String a4 = "ClicE";
		String b4 = "ClicE";
		String c4 = "Disparar";
		assertEquals(c4, Munonsito.funcion(a4,b4));
		
		String a5 = "ClicD";
		String b5 = "ClicD";
		String c5 = "Res";
		assertEquals(c5, Munonsito.funcion(a5,b5));
		
		String a6 = "ClicE";
		String b6 = "ClicD";
		String c6 = "Disparar";
		assertEquals(c6, Munonsito.funcion(a6,b6));
		
		String a7 = "ClicD";
		String b7 = "ClicE";
		String c7 = "Apuntar + Disparar";
		assertEquals(c7, Munonsito.funcion(a7,b7));
		
		String a8 = "w";
		String b8 = "s";
		String c8 = "Res";
		assertEquals(c8, Munonsito.funcion(a8,b8));
		
		String a9 = "s";
		String b9 = "w";
		String c9 = "Res";
		assertEquals(c9, Munonsito.funcion(a9,b9));
		
		String a10 = "a";
		String b10 = "d";
		String c10 = "Res";
		assertEquals(c10, Munonsito.funcion(a10,b10));
		
		String a11 = "d";
		String b11 = "a";
		String c11 = "Res";
		assertEquals(c11, Munonsito.funcion(a11,b11));
		
		String a12 = "w";
		String b12 = "d";
		String c12 = "w + d";
		assertEquals(c12, Munonsito.funcion(a12,b12));
		
		String a13 = "d";
		String b13 = "w";
		String c13 = "d + w";
		assertEquals(c13, Munonsito.funcion(a13,b13));
		
		String a14 = "w";
		String b14 = "a";
		String c14 = "w + a";
		assertEquals(c14, Munonsito.funcion(a14,b14));
		
		String a15 = "a";
		String b15 = "w";
		String c15 = "a + w";
		assertEquals(c15, Munonsito.funcion(a15,b15));
		
		String a16 = "a";
		String b16 = "s";
		String c16 = "a + s";
		assertEquals(c16, Munonsito.funcion(a16,b16));
		
		String a17 = "s";
		String b17 = "a";
		String c17 = "s + a";
		assertEquals(c17, Munonsito.funcion(a17,b17));
		
		
		
	}

}

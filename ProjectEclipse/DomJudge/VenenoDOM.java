import java.util.Scanner;

public class VenenoDOM {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int numero_veces = sc.nextInt();
		for (int j = 0; j < numero_veces; j++) {
			int hpmonstre = sc.nextInt();
			int atacjungla = sc.nextInt();
			int veri = sc.nextInt();
			int i = 0;
			boolean muertojungla = false;
			boolean muertoveri = false;
			while (!muertojungla && !muertoveri) {
				i++;
				hpmonstre = hpmonstre - atacjungla;
				if (hpmonstre <= 0) {
					muertojungla = true;
				} else {
					hpmonstre = hpmonstre - veri;
				}
				if (hpmonstre <= 0) {
					muertoveri = true;
				}
			}

			if (muertojungla) {
				System.out.println("RAMMUS " + i);

			} else if (muertoveri) {
				System.out.println("TWITCH " + i);
			}
		}

	}
}
